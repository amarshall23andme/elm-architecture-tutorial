import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Char

main =
  Html.beginnerProgram
    { model = model
    , view = view
    , update = update
    }



-- MODEL


type alias Model =
  { name : String
  , password : String
  , passwordAgain : String
  , age : String
  , reason : String
  , valid : Bool
  }


model : Model
model =
  Model "" "" "" "" "" False



-- UPDATE


type Msg
    = Name String
    | Password String
    | PasswordAgain String
    | Age String
    | Click Model

update : Msg -> Model -> Model
update msg model =
  case msg of
    Name name ->
      { model | name = name }

    Password password ->
      { model | password = password }

    PasswordAgain password ->
      { model | passwordAgain = password }

    Age age ->
      { model | age = age }

    Click model ->
      if model.password == model.passwordAgain then
        if String.length model.password > 7 then
          if String.any Char.isUpper model.password then
            if String.length model.age > 0 then
              if String.all Char.isDigit model.age then
                {model | valid = True, reason = "OK"}
              else
                {model | valid = False, reason = "Age is not a number"}
            else
              {model | valid = False, reason = "Age is not a number"}
          else
            {model | valid = False, reason = "Password does not contain an upper-case letter"}
        else
          {model | valid = False, reason = "Password less than 8 chars."}
      else
        {model | valid = False, reason = "Passwords do not match!"}


-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ input [ type_ "text", placeholder "Name", onInput Name ] []
    , input [ type_ "password", placeholder "Password", onInput Password ] []
    , input [ type_ "password", placeholder "Re-enter Password", onInput PasswordAgain ] []
    , input [ type_ "text", placeholder "Age", onInput Age ] []
    , button [ onClick (Click model) ] [ text "Validate" ]
    , viewValidation model
    ]

viewValidation : Model -> Html msg
viewValidation model =
  div [ style [("color", if model.valid then "green" else "red")] ] [ text model.reason ]
